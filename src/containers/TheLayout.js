import React from 'react'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'


export default class TheLayout extends React.Component {
  render() {
        return (
          <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader {...this.props} />
        <div className="c-body">
          <TheContent/>
        </div>
        <TheFooter/>
      </div>
    </div>)
  }
}
//
//
// const TheLayout = () => {
//
//   return (
//     <div className="c-app c-default-layout">
//       <TheSidebar/>
//       <div className="c-wrapper">
//         <TheHeader/>
//         <div className="c-body">
//           <TheContent/>
//         </div>
//         <TheFooter/>
//       </div>
//     </div>
//   )
// }

// export default TheLayout
