import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Physicals']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Datacenters',
    to: '/physicals/dcs',
    icon: 'cil-building',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Racks',
    to: '/physicals/racks',
    icon: 'cil-star',
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Hardware']
  },
  {
    _tag: 'CSidebarNavDivider'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Reports'],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Hardaware',
    route: '/',
    icon: 'cil-star',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'DC Capacity',
        to: '/',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Devices and Assets',
        to: '/',
      },

    ],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Network',
    icon: 'cil-building',
    route: '/pages',

  },
  {
    _tag: 'CSidebarNavDivider',
    className: 'm-2'
  },
  {
    _tag: 'CSidebarNavDivider',
    className: 'm-2'
  }
]

export default _nav
