import React from 'react'
import {
  CCol,
  CRow,
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
} from '@coreui/react'

import API from "../../api";


export default class RacksList extends React.Component {

  constructor(props) {
    super(props);
    this.history = props.history
    this.state = {
      racks: [],
    }
  }


  componentDidMount() {
    const { dc_id } = this.props.match.params

    API.get(`/api/v1/racks/?dc_guid=${dc_id}`)
      .then(response => {
        const racks = response.data.results;
        this.setState({racks})
      })
  }


  get_color_class (capacity) {
    let capacity_color = "primary"
    if (capacity <= 60 && capacity > 40) {
      capacity_color = "info"
    } else if (capacity <= 40 && capacity > 20) {
      capacity_color = "warning"
    } else if (capacity <= 20) {
      capacity_color = "danger"
    }
    return capacity_color
  }

  route_to(rack_id) {
    this.props.history.push(`/physicals/racks/${rack_id}`)
  }

  render () {
    return (
      <>
        <CRow>
          <CCol xs="12" lg="12">
            <CCard>
              <CCardHeader>
                Racks
              </CCardHeader>
              <CCardBody>
                <CDataTable striped
                  items={this.state.racks}
                  fields={['name', 'free_units', 'max_u_height', 'orientation_desc']}
                  itemsPerPage={5}
                  pagination
                  clickableRows={true}
                  onRowClick={(row) => this.route_to(row.id)}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )}
}
