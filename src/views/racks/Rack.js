import React from 'react'
import {
  CCol,
  CRow,
  CDataTable,
  CSpinner,
  CTooltip,
} from '@coreui/react'

import CIcon from "@coreui/icons-react";

import API from "../../api";


export default class Rack extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rack: {},
      isFetching: true
    };
  }

  componentDidMount() {
    const {rack_id} = this.props.match.params

    API.get(`/api/v1/racks/${rack_id}/`)
      .then(response => {
        const rack = response.data
        this.setState({rack: rack, isFetching: false})
      })
  }

  buildRackItem(item, item_type) {
    let f;
    if (item_type === 'accessory') {
      f = (
        <div className={`device accessory height-u-1 position-u-${item.position}`}/>
      )
    } else {
      f = (
        <div className={`device height-u-${item.height_of_device} position-u-${item.position}
                    half-slots`}>
          <div className="info">
                <span className="asset-description">{item.name}
                  {item.height_of_device <= 2 &&
                  <CTooltip content={`BC: ${item.barcode} SN: ${item.sn}`}>
                    <CIcon size={'sm'} name={'cilSun'} className={'has-tip'}/>
                  </CTooltip>
                  }
                </span>
            {item.height_of_device > 2 &&
            <span>
                  <div className="barcode">BC: {item.barcode}</div>
                  <div className="sn">SN: {item.sn}</div>
                </span>}
          </div>
        </div>
      )
    }
    return f
  }

  buildRack(side, assets, accessories) {
    return (
      <div className={`rack ${side} rack-u-${this.state.rack.max_u_height}`}>
        <div className="name">
          {this.state.rack.name}
        </div>
        <div className="wrapper">
          <div className="listing-u left">
            {
              [...Array(this.state.rack.max_u_height).keys()].map(i => i + 1).map((value, index) => {
                return <div key={index} className={`position position-u-${value}`}>{value}</div>
              })
            }
          </div>
          <div className="pdu left"/>
          <div className="devices">
            {assets.map(value => {
              return (
                this.buildRackItem(value, "device")
              )
            })}
            {accessories.map(value => {
              return (
                this.buildRackItem(value, "accessory")
              )
            })}
          </div>
          <div className="pdu right"/>
          <div className="listing-u right">
            {
              [...Array(this.state.rack.max_u_height).keys()].map(i => i + 1).map((value, index) => {
                return <div className={`position position-u-${value}`}>{value}</div>
              })

            }

          </div>
        </div>
      </div>
    )
  }

  render() {
    if (this.state.isFetching) {
      return <CSpinner color="info" variant="grow"/>
    }
    const front_assets = this.state.rack.root_assets.filter(accessory => accessory.orientation === "front")
    const front_accessories = this.state.rack.accessory.filter(accessory => accessory.orientation === "front")
    const back_assets = this.state.rack.root_assets.filter(accessory => accessory.orientation === "back")
    const back_accessories = this.state.rack.accessory.filter(accessory => accessory.orientation === "back")
    return (
      <CRow>
        <CCol xs="12" lg="4">
          <CDataTable
            header={false} border={true}
            items={[{'Name': 'Hostname', 'Value': 'ttt.local'}, {'Name': 'Model', 'Value': 'HP blabla'}]}
          >
          </CDataTable>
        </CCol>
        <CCol className="racks" xs="12" lg="4">
          {this.buildRack("front", front_assets, front_accessories)}
        </CCol>
        <CCol className="racks" xs="12" lg="4">
          {this.buildRack("back", back_assets, back_accessories)}
        </CCol>
      </CRow>


    )
  }
}
