import React from 'react'
import {
  CCol,
  CProgress,
  CRow,
  CWidgetDropdown, CDropdown, CDropdownToggle, CDropdownMenu, CDropdownItem
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import API from "../../api";


export default class Dashboard extends React.Component {
  state = {
    datacenters: [],
  }

  componentDidMount() {
    API.get("api/v1/data_centers/")
      .then(response => {
        const datacenters = response.data.results;
        this.setState({datacenters})
      })
  }

  get_color_class (capacity) {
    let capacity_color = "primary"
    if (capacity <= 60 && capacity > 40) {
      capacity_color = "info"
    } else if (capacity <= 40 && capacity > 20) {
      capacity_color = "warning"
    } else if (capacity <= 20) {
      capacity_color = "danger"
    }
    return capacity_color
  }
  render () {
  return (
    <>
      <CRow>
        {this.state.datacenters.map((value, index) => {
          return (
            <CCol sm="6" lg="3">
            <CWidgetDropdown
              color={`gradient-${this.get_color_class(value.capacity)}`}
              header={`${value.capacity}%`}
              text={value.name}
              footerSlot={
                <CProgress animated size="xs" className="my-3 mb-0 progress-white" value={value.capacity}/>
              }
            >
              <CDropdown>
                <CDropdownToggle color="transparent">
                  <CIcon name="cil-settings"/>
                </CDropdownToggle>
                <CDropdownMenu className="pt-0" placement="bottom-end">
                  <CDropdownItem>Go to DC</CDropdownItem>
                  <CDropdownItem href={`#/physicals/dcs/${value.id}/racks`} >DC Racks</CDropdownItem>
                </CDropdownMenu>
              </CDropdown>
            </CWidgetDropdown>
          </CCol>)
        })}
      </CRow>
    </>
  )}
}
