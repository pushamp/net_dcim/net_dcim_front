# stage1 as builder
FROM node:16-alpine as builder
WORKDIR /app
COPY package.json ./
RUN npm install

COPY . .

RUN npm run build


FROM nginx:alpine

COPY contrib/nginx/nginx.conf /etc/nginx/nginx.conf

RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 3000 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
